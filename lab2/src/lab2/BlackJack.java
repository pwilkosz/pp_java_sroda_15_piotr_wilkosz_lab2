package lab2;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class BlackJack {
	int[] talia;
	int PunktyGracza;
	int PunktyKrupiera;
	List<Integer> KartyGracza;
	List<Integer> KartyKrupiera;
	public BlackJack(){
		talia = new int[]{2,3,4,5,6,7,8,9,10,11};
		PunktyGracza = PunktyKrupiera = 0;
		KartyGracza = new ArrayList<Integer>();
		KartyKrupiera = new ArrayList<Integer>();
	}
	public int graj() throws IOException{
		
		//losowanie 2 kart
		Random gen = new Random();
		//dla gracza
		KartyGracza.add(gen.nextInt(9) + 2);
		KartyGracza.add(gen.nextInt(9) + 2);
		KartyKrupiera.add(gen.nextInt(9) + 2);
		KartyKrupiera.add(gen.nextInt(9) + 2);
		//policz punkty gracza
		PunktyGracza += (KartyGracza.get(0) + KartyGracza.get(1));
		PunktyKrupiera += (KartyKrupiera.get(0) + KartyKrupiera.get(1));
		
		System.out.println("Twoje Karty: " + KartyGracza.get(0) + " " + KartyGracza.get(1));
		System.out.println("Karta Krupiera: " + KartyKrupiera.get(0));
		
		while(PunktyGracza < 21 && PunktyKrupiera < 21){
			//spytaj gracza o dodatkowy ruch
			System.out.println("Czy chcesz dobrac karte? (t/n)");
			//Scanner S = new Scanner(System.in);
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String S = br.readLine();
			if(S.equals("t")){
				KartyGracza.add(gen.nextInt(9) + 2);
				PunktyGracza += KartyGracza.get(KartyGracza.size() - 1);
			}else{
				//dobieranie kart przez krupiera
				
				if(PunktyKrupiera < 16){
					//pobieramy karte
					KartyKrupiera.add(gen.nextInt(9) + 2);
					PunktyKrupiera += KartyKrupiera.get(KartyKrupiera.size() - 1);
				}
				if(PunktyKrupiera > PunktyGracza && PunktyKrupiera <= 21){
					System.out.println("Wygrywa Krupier");
				}
				else System.out.println("Wygrywasz!");
				return 0;
			}
			String kartyG = new String();
			
			
			for(int x: KartyGracza){
				kartyG += x + " ";
			}
			System.out.println("Twoje Karty: " + kartyG);
			System.out.println("Karta Krupiera: " + KartyKrupiera.get(0));
			
			//dobieranie kart przez krupiera
			
			if(PunktyKrupiera < 16){
				//pobieramy karte
				KartyKrupiera.add(gen.nextInt(9) + 2);
				PunktyKrupiera += KartyKrupiera.get(KartyKrupiera.size() - 1);
			}
				
		}
		if(PunktyGracza == 21){
			System.out.println("Wygrywasz!");
			return 0;
		}
		if(PunktyKrupiera == 21){
			System.out.println("Wygrywa Krupier");
			return 0;
		}
		//remis
		if(PunktyGracza == PunktyKrupiera){
			System.out.println("Wygrywa Krupier");
			return 0;
		}
		if(PunktyGracza > 21 ){
			System.out.println("Wygrywa Krupier");
			return 0;
		}
		if(PunktyKrupiera > 21 ){
			System.out.println("Wygrywasz");
			return 0;
		}
		return 0;
		
	}
}
